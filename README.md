C++ 20 compiler on Windows supports ALL C++20 language features, including concepts, modules, coroutines, compare, using enum, etc. (partial C++23 support)
https://en.cppreference.com/w/cpp/compiler_support

MinGW-w64 only download:
https://github.com/expnkx/mingw-gcc
or
https://bitbucket.org/ejsvifq_mabmip/mingw-gcc
or
https://gitee.com/qabeowjbtkwb/mingw-gcc